import React, { Component } from 'react';
import './App.css';

export default class SessionForm extends Component {
    constructor(props) {
      super(props);
      const oldName = props.session.name;
      this.state = {
        edited: false,
        newName: oldName
      }
    }
    handleChange(event, controller) {
      controller.setState({newName: event.target.value, edited: true});
    }
    render() {
      const { session, handlers } = this.props;
      const { edit, abort } = handlers;
      const { edited, newName } = this.state;
      return (
        <form>
          <input value={newName} onChange={e => {this.handleChange(e, this)} } />
          { edited 
            ? <div>
                <button onClick={ () => edit(session.id, 'name', newName) }>rename</button>
                <button onClick={ () => abort() }>abort</button>
              </div>
            : null
          }
        </form>
      )
    }
  }