import React, { Component } from 'react';
import fire from './fire';
import SessionList from './SessionList';
import SessionInfo from './SessionInfo';
import './App.css';
import { Navbar, NavbarGroup, NavbarDivider, NavbarHeading, Alignment, Button, Spinner } from "@blueprintjs/core";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessions: {
      },
      loaded: false,
      selectedSession: "",
      editableSession: "",
      showHomePage: false
    };
  }
  componentWillMount() {
    let hs_ref = fire.database().ref();
    let updateState = snapshot => {
      const s = snapshot.key;
      if (s === 'sessions') {
        const sessions = snapshot.toJSON();
        this.setState({
          sessions,
          loaded: true
        });
      }
    };
    hs_ref.on('child_added', updateState);
    hs_ref.on('child_changed', updateState);
    hs_ref.on('child_removed', updateState);
  }

  selectSession(selectedSession) {
    this.setState({
      selectedSession
    });
  }
  getSessionByID = id => this.state.sessions[id];
  getModelByID = id => {

    return this.state.sessions[this.state.selectedSession].models[id];
  }
  changeSessionProp(id, prop, value) {
    if (value !== undefined) {
      const newSession = Object.assign(
        {},
        this.getSessionByID(id),
        { [prop]: value }
      );
      let ref = fire.database()
        .ref(`sessions/${id}`)
        .set(newSession);
    }
  }
  changeModelProp(id, prop, value) {
    if (value !== undefined) {
      const oldModel = this.getModelByID(id);
      const newModel = Object.assign(
        {},
        this.getModelByID(id),
        { [prop]: value }
      );
      let ref = fire.database()
        .ref(`sessions/${this.state.selectedSession}/models/${id}`)
        .set(newModel);
    }
  }
  abortSessionEdit() {
    this.setState({
      editableSession: ""
    });
  }
  startSessionEdit(editableSession) {
    this.setState({ editableSession });
  }

  render() {
    const { sessions, loaded, selectedSession, editableSession, showHomePage } = this.state;
    const sessionData = Object.keys(sessions).map(s => Object.assign({
      id:s},
      sessions[s]
    ));
    const modelHandlers = {
      changeValue: (id, prop, value) => this.changeModelProp(id, prop, value)
    };
    const info = (selectedSession !== "")
      ? <SessionInfo session={this.getSessionByID(selectedSession)} handlers={modelHandlers} />
      : <span>Please select an AR session</span>;

    const handlers = {
      select: s => this.selectSession(s),
      startEdit: s => this.startSessionEdit(s),
      edit: (s, n, v) => this.renameSession(s, n, v),
      abort: (s) => this.abortSessionEdit()
    }
    const list = (
    <div className="content">
      {info}
      <SessionList
        sessions={sessionData}
        handlers={handlers}
        editable={editableSession}
        selected={selectedSession} />
      </div>
    );
    
    const sessionPage = (
      <div className="App-intro">
        <div>
          {loaded
            ? list
            : <Spinner small />
          }
        </div>
      </div>);

    const homePage = (
      <div className="App-intro">
        <h3>Welcome to smARt_OGs</h3>
        This an AR tool that allows collaborative exploration of 3D models via your smartphone.
To participate in the live demo, please install our app via ...
      </div>);

    return (
      <div className="App">
        <Navbar className="pt-dark">
            <NavbarGroup align={Alignment.LEFT}>
                <NavbarHeading>smARt_OGs</NavbarHeading>
                <NavbarDivider />
                <Button className="pt-minimal" icon="home" text="Home" onClick={() => {this.setState({showHomePage:true})}} />
                <Button className="pt-minimal" icon="document" text="Sessions" onClick={() => {this.setState({showHomePage:false})}}/>
            </NavbarGroup>
        </Navbar>
        { showHomePage
          ? homePage
          : sessionPage}
      </div>
    );
  }
}

export default App;
