import React from 'react';
import SessionForm from './SessionForm';
import { Button, ButtonGroup } from "@blueprintjs/core";

const SessionList = ({ sessions, handlers, selected, editable }) => (
    <ButtonGroup minimal vertical className="sessionList">
      <h6>AR Sessions</h6>
      {sessions.map(s => <Button
        key={s.id}
        onClick={() => handlers.select(s.id)}
        className={ selected === s.id ? 'selectedSession' : 'session' }>
          { editable === s.id 
            ? <SessionForm session={s} handlers={handlers} /> 
            : s.name
          }
        </Button>)}
    </ButtonGroup>
);
export default SessionList;