import React from 'react';
import { Slider, Icon, Card, Elevation } from "@blueprintjs/core";

export default ({model, handlers}) => {
    const { zoffset, rotation, scale } = model;
    const { changeValue } = handlers;
    return (
      <div className="modelForm">
        <Card elevation={Elevation.TWO}>
          <h4>{model.name}</h4>
          <div className="sliders">
            <div>
              <h6><Icon icon="refresh" /> rotation</h6>
              <Slider value={rotation} max={360} min={1} stepSize={1} labelStepSize={90} onChange={(e) => {changeValue(model.name, 'rotation', e)}}/>
            </div>
            <div>
              <h6><Icon icon="changes" /> offset</h6>
              <Slider value={zoffset} max={400} min={0} stepSize={1} labelStepSize={100} onChange={(e) => {changeValue(model.name, 'zoffset', e)}}/>
            </div>
            <div>
              <h6><Icon icon="maximize" /> scale</h6>
              <Slider value={scale} max={3} min={0} stepSize={0.0005} labelStepSize={1} onChange={(e) => {changeValue(model.name, 'scale', e)}}/>
            </div>
          </div>
        </Card>
      </div>
    );
}
  