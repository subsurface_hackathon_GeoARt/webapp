import React from 'react';
import ModelForm from './ModelForm.js'

export default ({session, handlers}) => (
      <div className="sessionInfo">
      {Object.entries(session.models).map(m => {
        const i = m[0];
        return <ModelForm model={session.models[i]} handlers={handlers} key={i} />
      })}
    </div>
);    